# snek


## Description
An ASCII art clone of the classic game [Snake](https://en.wikipedia.org/wiki/Snake_(video_game)) made using [Vite](https://vitejs.dev/) + Typescript + [Tailwind](https://tailwindcss.com/). Play it [here](https://eigelasinas.vercel.app/games/snek).


## Features
 - ASCII graphics
 - Fully responsive
 - Keyboard and touch controls
 - High score saved in localStorage
## 

## Controls
### Keyboard
 - ARROW KEYS - Movement
 - P - Pause
### Touch
 - SWIPE / ON-SCREEN ARROWS - Movement
 - TAP - Pause


## Screenshots
![mobile](images/mobile_screenshot.png)
![desktop](images/desktop_screenshot.png)

## Installation
```
    <!-- install dependencies -->
    npm install
    <!-- run development server -->
    npm run dev
    <!-- build and preview -->
    npm run build
    npm run preview
```

## How to Enjoy
Play until snek takes over your life. We all live to serve and feed the almighty snek.
