import { z } from "zod"
import Game from "./game"
import Vec from "./vec"

type GameState = {
	snake: Vec[]
	food: Vec
	direction: Vec
	directionQueue: Vec[]
	score: number
	highScore: number
	justDied: boolean
}

const JUST_DIED_TIMER = 500

const SaveData = z.object({
	highScore: z.number(),
})

type SaveData = z.infer<typeof SaveData>

export default class Snake extends Game {
	state: GameState
	cellSize = 50
	fps = 8
	screenSize = new Vec(15, 15)

	constructor(canvas: HTMLCanvasElement, public localStorageKey?: string) {
		super(canvas)

		this.state = {
			snake: [
				new Vec(
					Math.floor(this.screenSize.x / 2),
					Math.floor(this.screenSize.y / 2)
				),
			],
			food: new Vec(0, 0),
			direction: Vec.ZERO,
			directionQueue: [],
			score: 0,
			highScore: 0,
			justDied: false,
		}

		if (localStorageKey) {
			const res = SaveData.safeParse(
				JSON.parse(localStorage.getItem(localStorageKey) || "{}")
			)
			if (res.success) {
				this.state.highScore = res.data.highScore
			}
		}
	}

	init() {
		this.reset()
	}

	die() {
		this.state.justDied = true
		this.reset()
		setTimeout(() => {
			this.state.justDied = false
		}, JUST_DIED_TIMER)
	}

	reset() {
		this.state.snake = [
			new Vec(
				Math.floor(this.screenSize.x / 2),
				Math.floor(this.screenSize.y / 2)
			),
		]
		this.state.direction = Vec.ZERO
		this.state.score = 0
		this.setFoodPosition()
	}

	setFoodPosition() {
		const validCells = Array.from({ length: this.screenSize.x }, (_, x) =>
			Array.from({ length: this.screenSize.y }, (_, y) => new Vec(x, y))
		)
			.flat()
			.filter(
				(cell) =>
					cell.x !== 0 &&
					cell.x !== this.screenSize.x - 1 &&
					cell.y !== 0 &&
					cell.y !== this.screenSize.y - 1 &&
					!this.state.snake.find(
						(snakeCell) => snakeCell.x === cell.x && snakeCell.y === cell.y
					)
			)

		this.state.food = validCells[Math.floor(Math.random() * validCells.length)]!
	}

	handleInput(e: KeyboardEvent) {
		if (this.status === "paused") {
			return
		}

		if (this.state.justDied) {
			return
		}

		switch (e.key) {
			case "ArrowUp":
				if (this.state.direction === Vec.DOWN) return
				this.state.directionQueue.push(Vec.UP)
				break
			case "ArrowDown":
				if (this.state.direction === Vec.UP) return
				this.state.directionQueue.push(Vec.DOWN)
				break
			case "ArrowLeft":
				if (this.state.direction === Vec.RIGHT) return
				this.state.directionQueue.push(Vec.LEFT)
				break
			case "ArrowRight":
				if (this.state.direction === Vec.LEFT) return
				this.state.directionQueue.push(Vec.RIGHT)
				break
		}
	}

	update() {
		let nextDirection = this.state.directionQueue.shift()

		while (nextDirection) {
			if (
				!this.state.direction.eq(nextDirection) &&
				!this.state.direction.neg().eq(nextDirection)
			) {
				this.state.direction = nextDirection
				break
			}

			nextDirection = this.state.directionQueue.shift()
		}

		const { snake, food, direction } = this.state

		snake.unshift(snake[0]!.add(direction))

		const onWall =
			snake[0]!.x === 0 ||
			snake[0]!.y === 0 ||
			snake[0]!.x === this.screenSize.x - 1 ||
			snake[0]!.y === this.screenSize.y - 1
		const onFood = snake[0]!.x === food.x && snake[0]!.y === food.y
		const onSnake =
			snake.length > 2 &&
			snake
				.slice(1)
				.some((cell) => snake[0]!.x === cell.x && snake[0]!.y === cell.y)

		if (onWall || onSnake) this.die()
		else if (onFood) {
			this.setFoodPosition()
			this.state.score++
			if (this.state.score > this.state.highScore) {
				this.state.highScore = this.state.score
				const saveData: SaveData = {
					highScore: this.state.highScore,
				}

				if (this.localStorageKey) {
					localStorage.setItem(this.localStorageKey, JSON.stringify(saveData))
				}
			}
		} else {
			snake.pop()
		}
	}

	render() {
		const { snake, food } = this.state

		this.ctx().clearRect(0, 0, this.canvas.width, this.canvas.height)

		const scoreText = `Score:${this.state.score}`
		const highScoreText = `HiScore:${this.state.highScore}`

		Array.from({ length: this.screenSize.x }, (_, x) => {
			Array.from({ length: this.screenSize.y }, (_, y) => {
				if (y === 0 && x >= 1 && x <= scoreText.length) {
					this.setCell(new Vec(x, y), scoreText[x - 1]!, "#cf7878")
				} else if (
					y === this.screenSize.x - 1 &&
					x >= 1 &&
					x <= highScoreText.length
				) {
					this.setCell(new Vec(x, y), highScoreText[x - 1]!, "#cf7878")
				} else if (snake[0]!.x === x && snake[0]!.y === y) {
					this.setCell(new Vec(x, y), "O", "#c5a2a2")
				} else if (snake.some((cell) => cell.x === x && cell.y === y)) {
					this.setCell(new Vec(x, y), "o", "#c5a2a2")
				} else if (
					x === 0 ||
					x === this.screenSize.x - 1 ||
					y === 0 ||
					y === this.screenSize.y - 1
				) {
					this.setCell(new Vec(x, y), "#", "#c5a2a2")
				} else if (x === food.x && y === food.y) {
					this.setCell(new Vec(x, y), "$", "#cf7878")
				} else {
					this.setCell(new Vec(x, y), ".", "#312a3c")
				}
			})
		})
	}
}
