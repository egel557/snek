export default class Vec {
	static ZERO = new Vec(0, 0)
	static UP = new Vec(0, -1)
	static DOWN = new Vec(0, 1)
	static LEFT = new Vec(-1, 0)
	static RIGHT = new Vec(1, 0)

	constructor(public x: number, public y: number) {}

	public add(b: Vec): Vec {
		return new Vec(this.x + b.x, this.y + b.y)
	}

	public sub(b: Vec): Vec {
		return new Vec(this.x - b.x, this.y - b.y)
	}

	public neg(): Vec {
		return new Vec(-this.x, -this.y)
	}

	public eq(b: Vec): boolean {
		return this.x === b.x && this.y === b.y
	}
}
