type direction = "left" | "right" | "up" | "down"
const MOVE_THRESHOLD = 20

export default function addSwipeListener(
	container: HTMLElement,
	onSwipe: (direction: direction) => void
): void {
	let touchStart: Touch
	let isSwipeHandled = false

	container.addEventListener("touchstart", (e) => {
		touchStart = e.touches[0]!
		isSwipeHandled = false
	})

	container.addEventListener("touchmove", (e) => {
		e.preventDefault()
		e.stopPropagation()
		if (isSwipeHandled) return

		const distanceX = touchStart.clientX - e.touches[0]!.clientX
		const distanceY = touchStart.clientY - e.touches[0]!.clientY

		const shouldSwipe =
			Math.abs(distanceX) >= MOVE_THRESHOLD ||
			Math.abs(distanceY) >= MOVE_THRESHOLD

		if (!shouldSwipe) return

		isSwipeHandled = true

		const isHorizontal = Math.abs(distanceX) > Math.abs(distanceY)

		if (isHorizontal) {
			if (distanceX < 0) {
				onSwipe("right")
			} else {
				onSwipe("left")
			}
		} else {
			if (distanceY < 0) {
				onSwipe("down")
			} else {
				onSwipe("up")
			}
		}
	})
}
