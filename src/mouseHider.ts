export default function addMouseHider(container: HTMLElement) {
	let idleMouseTimer: number | undefined
	container.style.cursor = "none"
	container.addEventListener("mousemove", () => {
		if (idleMouseTimer !== undefined) {
			container.style.cursor = "auto"
			window.clearTimeout(idleMouseTimer)
		}

		idleMouseTimer = window.setTimeout(() => {
			container.style.cursor = "none"
		}, 1000)
	})
}
