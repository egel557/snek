import Vec from "./vec"

export default abstract class Game {
	abstract fps: number
	abstract screenSize: Vec
	abstract cellSize: number
	public status: "initial" | "running" | "paused" = "initial"

	constructor(public canvas: HTMLCanvasElement) {}

	protected abstract init(): void
	protected abstract update(): void
	protected abstract render(): void
	public abstract handleInput(e: KeyboardEvent): void

	protected msPerUpdate(): number {
		return 1000 / this.fps
	}
	protected ctx(): CanvasRenderingContext2D {
		return this.canvas.getContext("2d")!
	}

	public pause() {
		if (this.status === "paused") return
		this.status = "paused"
	}

	public resume() {
		if (this.status !== "paused") return
		this.status = "running"
	}

	async start() {
		this.canvas.style.backgroundColor = "rgba(0,0,0,0.75)"
		this.canvas.focus()

		const screenSize = this.screenSize
		const cellSize = this.cellSize

		const ratio = window.devicePixelRatio
		this.canvas.width = screenSize.x * cellSize * ratio
		this.canvas.height = screenSize.y * cellSize * ratio

		this.ctx().scale(ratio, ratio)

		this.ctx().font = `bold ${cellSize}px monospace`
		await new Promise((resolve) => {
			setTimeout(resolve, 100)
		})

		this.init()

		this.status = "running"

		window.requestAnimationFrame((timestamp) => {
			this.loop(timestamp, timestamp, 0)
		})
	}

	private loop(previous: number, current: number, lag: number) {
		const elapsed = current - previous
		previous = current
		lag += elapsed

		while (lag >= this.msPerUpdate()) {
			if (this.status === "running") {
				this.update()
			}
			lag -= this.msPerUpdate()
		}

		this.render()

		window.requestAnimationFrame((timestamp) => {
			this.loop(previous, timestamp, lag)
		})
	}

	protected cellToCanvas(cell: Vec) {
		return new Vec(
			cell.x * this.cellSize + this.cellSize / 5,
			cell.y * this.cellSize + this.cellSize / 1.2
		)
	}
	protected setCell(cell: Vec, char: string, color = "white") {
		cell = structuredClone(cell)
		cell = this.cellToCanvas(cell)
		this.ctx().fillStyle = color

		this.ctx().fillText(char, cell.x, cell.y)
	}
}
