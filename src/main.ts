import "@total-typescript/ts-reset"
import Snake from "./snake"
import raise from "./raise"
import addMouseHider from "./mouseHider"
import addSwipeListener from "./swipeListener"

const canvas = (document.getElementById("canvas") ??
	raise("Canvas not found")) as HTMLCanvasElement
const wrapper =
	(document.getElementById("wrapper") as HTMLElement) ??
	raise("Wrapper not found")

const urlParams = new URLSearchParams(window.location.search)
const localStorageKey = urlParams.get("localStorageKey")

const game = new Snake(canvas, localStorageKey || "saveData")

window.addEventListener("keydown", (e) => {
	if (e.key === "F12" || e.key === "F5") return
	if (e.key === "p" && game.status === "running") {
		game.pause()
		document.getElementById("pause-screen")?.classList.add("show")
	} else if (e.key === "p" && game.status === "paused") {
		game.resume()
		document.getElementById("pause-screen")?.classList.remove("show")
	} else if (game.status === "running") {
		game.handleInput(e)
	} else {
		return
	}
	e.preventDefault()
	e.stopPropagation()
	e.stopImmediatePropagation()
})

document.getElementById("arrow-up")?.addEventListener("click", () => {
	window.dispatchEvent(new KeyboardEvent("keydown", { key: "ArrowUp" }))
})

document.getElementById("arrow-down")?.addEventListener("click", () => {
	window.dispatchEvent(new KeyboardEvent("keydown", { key: "ArrowDown" }))
})

document.getElementById("arrow-left")?.addEventListener("click", () => {
	window.dispatchEvent(new KeyboardEvent("keydown", { key: "ArrowLeft" }))
})

document.getElementById("arrow-right")?.addEventListener("click", () => {
	window.dispatchEvent(new KeyboardEvent("keydown", { key: "ArrowRight" }))
})

addSwipeListener(wrapper, (direction) => {
	window.dispatchEvent(
		new KeyboardEvent("keydown", {
			key: {
				up: "ArrowUp",
				down: "ArrowDown",
				left: "ArrowLeft",
				right: "ArrowRight",
			}[direction],
		})
	)
})

wrapper.addEventListener("click", () => {
	window.dispatchEvent(new KeyboardEvent("keydown", { key: "p" }))
})

if (!("ontouchstart" in window)) {
	addMouseHider(document.body)
}

game.start()
