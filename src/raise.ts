export default function raise(message: string): never {
	throw new Error(message)
}
